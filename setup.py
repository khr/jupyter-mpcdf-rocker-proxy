import setuptools

setuptools.setup(
    name="jupyter-mpcdf-rocker-proxy",
    version='0.0.1',
    url="https://gitlab.mpcdf.mpg.de/khr/jupyter-mpcdf-rocker-proxy",
    author="Klaus Reuter",
    description="klaus.reuter@mpcdf.mpg.de",
    packages=setuptools.find_packages(),
	keywords=['Jupyter'],
	classifiers=['Framework :: Jupyter'],
    install_requires=[
        'jupyter-server-proxy'
    ],
    entry_points={
        'jupyter_serverproxy_servers': [
            'mpcdf-rocker = jupyter_mpcdf_rocker_proxy:setup_mpcdf_rocker_proxy',
        ]
    },
    package_data={
        'jupyter_mpcdf_rocker_proxy': ['icons/*'],
    },
)
