"""
Return config on servers to start web services from JupyterLab

See https://jupyter-server-proxy.readthedocs.io/en/latest/server-process.html
for more information.
"""
import os

# a proxy to enable simple mpcdf-rockering of files via http
def setup_mpcdf_rocker_proxy():
    return {
        'command': [
            "python3", "-m", "http.server", "{port}"
        ],
        'environment': {},
        'timeout': 60.0,
        'launcher_entry': {
            'title': 'RStudio (Rocker)',
            'icon_path': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'icons', 'rocker.png'),
        }
    }
